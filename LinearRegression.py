import numpy as np
import pandas as pd
from sklearn.datasets import load_diabetes
import matplotlib.pyplot as plt
plt.rcParams['figure.figsize'] = (12.0, 9.0)


class LinearRegression:
	def __init__(self, x, y):
		"""
		type(x) = ndArray
		type(y) = ndArray
		"""
		if isinstance(x, list):
			x = np.array(x)
		if isinstance(y, list):
			y = np.array(y)
		self.x, self.y = x, y
		self.mean_x, self.mean_y = np.mean(self.x), np.mean(self.y)
		# model parameters
		self.gradient = None	# m (or slope)
		self.bias = None	# c (or y-intercept)
	

	def fit(self):
		"""
		train the Linear Regression model.
		"""
		numerator = np.sum((self.x - self.mean_x) * (self.y - self.mean_y))
		denominator = np.sum((self.x - self.mean_x) ** 2)
		# calculation of slope
		self.gradient = numerator / denominator
		# calculation of intercept.
		self.bias = self.mean_y - (self.gradient * self.mean_x)
		return self.gradient, self.bias
	
	def predict(self, x):
		"""
		predicts the value.
		"""
		if self.gradient is None or self.bias is None:
			raise Exception("Instance not initialised")
		return (self.gradient * x) + self.bias
	
	def plot(self, x_label, y_label, offset=None):
		"""
		plot the original and predicted values
		offset: This param is used from where to start and end the plot.
		For example: if min_x = 100, max_y=100 and offset is 10
		then the plot will start from 90 and end at 110
		"""
		plt.scatter(self.x, self.y)
		if offset is None:
			plt.plot(self.x, self.predict(self.x), color='red')
		else:
			x_values = np.linspace(np.min(self.x) - offset, np.max(self.x) + offset)
			plt.plot(x_values, self.predict(x_values), color='red')
		plt.xlabel(x_label)
		plt.ylabel(y_label)
		plt.show()
	

	def error_func(self, func_name='rms'):
		if func_name in [None, "", "null", "None"]:
			raise Exception("")
		if func_name == 'rms':
			return self.rms_error()
		elif func_name == 'r2':
			return self.r2_error()
	
	def rms_error(self):
		rms = (np.sum((self.predict(self.x) - self.y) ** 2) / len(self.y)) ** 0.5
		return rms
	

	def r2_error(self, percents=False):
		sst = np.sum((self.y - self.mean_y) ** 2)
		ssr = np.sum((self.mean_y - self.predict(self.x)) ** 2)
		if percents:
			return (1 - (ssr/sst)) * 100
		return 1 - (ssr/sst)


def read_diabetes_data():
	# ['age', 'sex', 'bmi', 'bp', 's1', 's2', 's3', 's4', 's5', 's6']
	x = load_diabetes(return_X_y=True)[0]
	bmi, bp = x[:,2], x[:,3]
	return bmi, bp


def main():
	"""
	function to plot best line of fit for
	bmi as independant variable (x-axis)
	bp as dependant variable (y-axis)
	"""
	bmi, bp = read_diabetes_data()
	classifier = LinearRegression(bmi, bp)
	# from y = mx + c
	m, c = classifier.fit()
	# Root mean square Error
	rms_error = classifier.error_func()
	# R2 error
	r2_erro = classifier.error_func(func_name='r2')

	print("Slope: {0}, Bias: {1}".format(m, c))
	print("RMS Error: {0}".format(rms_error))
	print("R2 error: {0}".format(r2_erro))
	# plot the graph
	classifier.plot(x_label='Body Mass Index', y_label='Blood Pressure', offset=0.001)


if __name__ == "__main__":
	main()
