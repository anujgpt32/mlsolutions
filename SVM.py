import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt


class SupportVector:

    def __init__(self):
        self.x_train, self.x_test, self.y_train, self.y_test = None, None, None, None
        """
        linear: 100%,
        poly: 99.59%
        rbf: 78%
        sigmoid: 46%
        """
        self.model = SVC(gamma='auto', kernel='linear')
        self.df = None
        self.read_csv()
        self.model.fit(self.x_train, self.y_train)

    def under_sampling(self, df):
        """
        function to balance the classes.
        :param df:
        :return:
        """
        # separating classes
        frauds = df[df['Class'] == 1]
        frauds = frauds.sample(frac=1)
        non_frauds = df[df['Class'] == 0]
        # non_frauds = non_frauds.sample(frac=1)
        # under sampling the non frauds data as it is in abundance
        non_frauds = non_frauds[:492]
        # concatenating
        new_df = pd.concat([frauds, non_frauds])
        del df, non_frauds, frauds
        # shuffling the data set
        new_df = new_df.sample(frac=1).reset_index(drop=True)
        return new_df

    def predict(self, x_test):
        return self.model.predict(x_test)

    def accuracy_score(self):
        y_pred = self.predict(self.x_test)
        return accuracy_score(self.y_test, y_pred)

    def read_csv(self):
        df = pd.read_csv("./datasets/creditcard.csv")
        # since the classes 0, 1 are highly unbalanced. under sampling of abundant class is performed
        df = self.under_sampling(df)
        self.df = df
        y = df['Class']
        del df['Class']
        x = df.values
        # separate training and test data
        self.x_train, self.x_test, self.y_train, self.y_test = train_test_split(x, y)
        del df

    def plot(self, columns):
        self.df.hist(columns)
        plt.show()
        return 0


def main():
    instance = SupportVector()
    # instance.plot(['Amount'])
    print("Accuracy: ", instance.accuracy_score() * 100)


if __name__ == '__main__':
    main()
