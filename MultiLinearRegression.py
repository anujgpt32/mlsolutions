import numpy as np
import pandas as pd
from sklearn.datasets import load_diabetes
from sklearn.model_selection import train_test_split

"""
X1 = total overall reported crime rate per 1 million residents
X2 = reported violent crime rate per 100,000 residents
X3 = annual police funding in $/resident
X4 = % of people 25 years+ with 4 yrs. of high school
X5 = % of 16 to 19 year-olds not in highschool and not highschool graduates.
X6 = % of 18 to 24 year-olds in college
X7 = % of people 25 years+ with at least 4 years of college

Objective:
Using X3, X4, X5, X6 and X7, X1 will be calculated.
"""


class MultiLinearRegression:
	def __init__(self, x_train, y_train, x_test, y_test):
		"""
		type(x) = ndArray
		type(y) = ndArray
		"""
		self.x_train, self.y_train = x_train, y_train
		self.x_test, self.y_test = x_test, y_test
		# stores the slopes for the features X3, X4, X5, X6 and X7 respectively.
		self.biases = np.zeros(len(self.x_train))
		self.cost_history = list()
	
	def fit(self, epochs, learning_rate):
		"""
		train the Linear Regression model.
		epochs: No. of iterations.
		learning_rate: rate of convergence to optimum solution.
		"""
		for i in range(epochs):
			m = len(self.x_train)
			h = self.x_train.T.dot(self.biases)
			loss = h - self.y_train
			gradient = self.x_train.dot(loss) / m
			print(self.biases.reshape(5, 1))
			self.biases = self.biases - learning_rate * gradient
			# New Cost Value
			cost = self.cost_function()
			self.cost_history.append(cost)
		return self.biases, self.cost_history
	
	def cost_function(self):
		m = len(self.y_train)
		return np.sum((self.x_train.T.dot(self.biases) - self.y_train) ** 2)/(2 * m)
	
	def predict(self, x):
		"""
		predicts the value.
		"""
		return 0	

	def error_func(self, func_name='rms'):
		if func_name in [None, "", "null", "None"]:
			raise Exception("")
		if func_name == 'rms':
			return self.rms_error()
		elif func_name == 'r2':
			return self.r2_error()
	
	def rms_error(self):
		rms = (np.sum((self.predict(self.x) - self.y) ** 2) / len(self.y)) ** 0.5
		return rms
	

	def r2_error(self, percents=False):
		sst = np.sum((self.y - self.mean_y) ** 2)
		ssr = np.sum((self.mean_y - self.predict(self.x)) ** 2)
		if percents:
			return (1 - (ssr/sst)) * 100
		return 1 - (ssr/sst)


def read_crime_data():
	"""
	reads the crime data of multiple cities and splits them into train-test data.
	"""
	df = pd.read_excel('./datasets/mlr06.xls')
	features = np.array(df[['X3', 'X4', 'X5', 'X6', 'X7']].values.tolist())
	labels = np.array(df[['X1']].values.tolist())
	x_train, x_test, y_train, y_test = train_test_split(features, labels)
	return x_train.T, x_test.T, y_train, y_test


def main():
	"""
	function to plot best line of fit for
	bmi as independant variable (x-axis)
	bp as dependant variable (y-axis)
	"""
	x_train, x_test, y_train, y_test = read_crime_data()
	classifier = MultiLinearRegression(x_train, y_train, x_test, y_test)
	coeffs, cost_history = classifier.fit(350, 0.001)


if __name__ == "__main__":
	main()
