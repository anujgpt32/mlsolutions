from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import pandas as pd


def convert_grade(grade):
    if "A++ " == grade:
        grade = 1
    elif "A+ " == grade:
        grade = 2
    elif "A " == grade:
        grade = 3
    elif "A- " == grade:
        grade = 4
    elif "B+ " in grade:
        grade = 5
    return grade


def read_csv():
    df = pd.read_csv("./datasets/data.csv", encoding="utf8")
    # dropping unnecessary columns
    del df['rank']
    del df['channel']
    df = df.loc[(df['views'] != '-- ') & (df['views'] != '--')]
    df = df.loc[(df['subscribers'] != '-- ') & (df['subscribers'] != '--')]
    df = df.loc[(df['uploads'] != '-- ') & (df['uploads'] != '--')]
    df = df.loc[(df['grade'] != '-- ') & (df['grade'] != '\xa0')]

    df['uploads'] = df['uploads'].astype(int)
    # df['views'] = df['views'].astype(int)
    df['subscribers'] = df['subscribers'].astype(int)

    df = df.dropna()
    df['grade'] = df['grade'].apply(lambda x: convert_grade(x))

    x = df[["uploads", "subscribers", "views"]].values.tolist()
    y = df['grade'].values.tolist()
    return x, y


def main():
    history = []
    x, y = read_csv()
    x_train, x_test, y_train, y_test = train_test_split(x, y, random_state=2)
    max = 0
    max_k = None
    k_range = range(1, 101)

    for k in k_range:
        models = KNeighborsClassifier(n_neighbors=k)
        models.fit(x_train, y_train)
        predictions = models.predict(x_test)
        # print(predictions)
        x = accuracy_score(y_test, predictions) * 100
        history.append(x)
        # print("{0:.4f}%".format(x))
        if x > max:
            max = x
            max_k = k
        del models

    print("Max accuracy and k-value:")
    print(max, max_k)
    del x, y
    del x_train, x_test, y_train, y_test
    plt.plot(k_range, history)
    plt.show()
    return 0


main()
